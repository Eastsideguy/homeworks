const arr1 = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
const arr2 = ['1', '2', '3', 'sea', 'user', 23];

function printElem(arr){
    const arrLi = arr.map(function (elem) {
        return `<li>${elem}</li>`
    });
    const ul = document.createElement('ul');
    document.body.appendChild(ul);
    for(let i = 0; i < arr.length; i++){
       ul.innerHTML += arrLi[i];
    }
}
printElem(arr1);
printElem(arr2);
