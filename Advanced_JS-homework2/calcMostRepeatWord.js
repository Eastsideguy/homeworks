
let popularWord;
let popularWordCount = 0;


calcMostPopularWord('Lorem ipsum quia dolor sit amet, consectetur adipisicing elit. Aliquid, quia! voluptate. Alias at deleniti ea, error facilis laudantium molestiae neque nulla omnis pariatur perspiciatis quaerat quasi quia tempora ullam veritatis, voluptas! Cupiditate doloribus molestiae pariatur porro saepe tenetur velit. Alias consequatur distinctio eveniet in, labore mollitia quae repudiandae sunt veritatis Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, quia voluptate. Alias at deleniti ea, error facilis laudantium molestiae neque nulla omnis pariatur perspiciatis quaerat quasi quia tempora ullam veritatis!');

console.log(popularWord);
console.log(popularWordCount);


function calcMostPopularWord(phrase){
    let phraseReplaced = phrase.replace(/[^\w\s]/g, '');
    const phraseWordsArr = phraseReplaced.split(' ');

    const counts = {};
    for (let i = 0, len = phraseWordsArr.length; i < len; i++){
        let word = phraseWordsArr[i];
        if (counts[word] === undefined) {
            counts[word] = 1;
        }else {
            counts[word] = counts[word] + 1;
        }
        if(counts[word] > popularWordCount){
            popularWordCount = counts[word];
            popularWord  = phraseWordsArr[i];
        }
    }
return  popularWord;
}
