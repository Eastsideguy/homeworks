const inputField = document.getElementById('priceField');
const nowPrice = document.getElementById('price-now');
const topPrice = document.getElementById('tooltip');
const error = document.getElementById('uncorrect-value');
const cross = document.getElementById('cross');

inputField.addEventListener('focusout', function () {

    showPrice(this)
});
inputField.addEventListener('focusin', colorFocus);
cross.addEventListener('click', hidePrice);
function showPrice(elem) {
    if(elem.value >= 0) {
       elem.style.color = "limegreen";
        nowPrice.innerHTML = `Текущая цена ${elem.value}`;
        topPrice.style.visibility = "visible";
        error.style.visibility = "hidden";
        elem.style.outline = 'none';
    }else {
        error.style.visibility = "visible";
        elem.style.outline = "3px solid red";
        elem.style.color = 'black';
        topPrice.style.visibility = "hidden";

    }

}

function colorFocus() {
    inputField.style.outline = "3px solid limegreen";

}

function hidePrice() {
    topPrice.style.visibility = "hidden";
    inputField.value = '';
    error.style.visibility = "hidden";

}

