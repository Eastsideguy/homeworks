let newArray = [1, "I'm", 23, true, {name: "Oleksii"}, null, undefined ];

function filterBy(arr, type) {
        return arr.filter(function (item) {
            if (typeof item !== type)
                return item
        })
}

console.log(filterBy(newArray, 'string'));
console.log(filterBy(newArray, 'null'));
console.log(filterBy(newArray, 'object'));
console.log(filterBy(newArray, 'number'));
console.log(filterBy(newArray, 'undefined'));
console.log(filterBy(newArray, 'boolean'));

