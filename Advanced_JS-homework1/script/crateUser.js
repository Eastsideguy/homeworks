const user1 = createUser('Oleksii', 'Kovtun', 'age: 29', 'weight: 70','height: 176', 'nationality: Ukrainian');
console.log(user1);


function createUser(userName, userSurname, ...properties) {
    const user = {
        name: userName,
        "last name": userSurname,
    };
    const propArr = [...properties];
    const propValues = [];

    propArr.forEach( elem => {
        const newElem = elem.split(': ');
        propValues.push(newElem);
        propValues.forEach(elem => user[elem[0]] = elem[1])
    });

    return user;
}