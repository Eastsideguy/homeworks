const tabs = document.getElementsByClassName('tabs-title');
const allCont = document.querySelectorAll('.content');

for (let i = 0; i < tabs.length; i++) {
    tabs[i].addEventListener('click', function () {
        switchTab(this);
        switchCont(this, allCont);
    });
}

function switchTab(elem) {
    let tab = elem.previousElementSibling;
    while (tab) {
        tab.classList.remove('active');
        tab = tab.previousElementSibling;
    }

    tab = elem.nextElementSibling;
    while (tab) {
        tab.classList.remove('active');
        tab = tab.nextElementSibling;
    }
    elem.classList.add('active');
}

function switchCont(elem, allTabs) {
    const id = elem.dataset.cont;
    const content = document.getElementById(id);
    for (let i = 0; i < allTabs.length; i++) {
        allTabs[i].style.display = 'none'
    }
    content.style.display = "block";
}