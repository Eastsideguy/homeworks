function fibo(F0 = 1,F1 = 1, n) {
    if(n > 1) {
        for (let i = 3; i <= n; i++) {
            let F2 = F1 + F0;
            F0 = F1;
            F1 = F2;
        }
        return F1;
    }else if (n < 0) {
        F0 = -1;
        F1 = -1;

        for (let i = -3; i >= n; i--) {
            let F2 = F0 + F1;
            F0 = F1;
            F1 = F2;
        }
        return F1;
    }
}
let n = +prompt("Please, enter n");
console.log(fibo(1,1, n));