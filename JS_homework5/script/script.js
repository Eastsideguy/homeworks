
function createNewUser() {
    const newUser = {
        firstName: prompt("Please, enter First Name"),
        lastName: prompt("Please, enter Last Name"),
        birthday: prompt("Please, enter your birthday",'dd.mm.yyyy'),
        getLogin: function () {
            let login =  this.firstName[0] + this.lastName;
            return login.toLocaleLowerCase()
        },
        getAge: function () {
            let now = new Date();
            let age = now.getFullYear() - (this.birthday.slice(6,11));
            if( now.getMonth() < (this.birthday.slice(3,5) - 1) ){
                return age - 1;
            }else if(now.getMonth() === (this.birthday.slice(3,5) - 1) && now.getDate() < this.birthday.slice(0,2)){
               return age -1;
            }else {
                return age;
            }


        },
        getPassword: function () {
            let password = this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6,11);
            return password;
        }
    };
    return newUser;
}

let user1 = createNewUser();
console.log(user1);
console.log(user1.getLogin());
console.log(user1.getAge());
console.log(user1.getPassword());
